import React, {Component} from "react";
import fetchJsonp from "fetch-jsonp";
import Oauth from "./Oauth";

function rememberToken() {
    let location = window.location.href;
    let accessToken = location.slice(location.indexOf('=')+1, location.indexOf('&'));
    localStorage.setItem("accessToken", accessToken);
    console.log(accessToken);
}

class Friend extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error : null,
            isLoaded : false,
            friends : []
        };
    }
    componentDidMount() {
        if(!localStorage.getItem("accessToken")){
            rememberToken();
        }
      //  else {
      //      localStorage.setItem("redirect", JSON.stringify(false))
      //  }
        console.log(localStorage);
        let friend_url = new URL('https://api.vk.com/method/friends.get?access_token='+localStorage.getItem("accessToken")+'&v=5.74&count=5&order=random&fields=city');
        fetchJsonp(friend_url.toString(),{
            method:'GET'
        })
            .then(response => response.json())
            .then(
                (result) => {
                    if ("error" in result){
                        if (result.error.error_code === 5) {
                            localStorage.removeItem("accessToken");
                            localStorage.setItem("redirect", JSON.stringify(false))
                        }
                    } else {
                        this.setState({
                            isLoaded: true,
                            friends: result.response.items
                        })
                    }
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {
        const {error, isLoaded, friends } = this.state;
        if (error) {
            return <div>Ошибка: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Загрузка...</div>;
        }
        else if (!JSON.parse(localStorage.getItem("redirect"))) {
            return (
                < Oauth/>
            )
        }
        else {
            return (
                <div className="card-body">
                    <p className="card-header"> Ваши друзья :</p>
                    <ul className="list-group list-group-flush">
                        {friends.map(friend => (
                            <li className="list-group-item" key={friend.id}>
                                {friend.first_name} {friend.last_name}
                            </li>
                        ))}
                    </ul>
                </div>

            )
        }
    }
}
export default Friend