import React, {Component} from "react";
import Friend from "./Friend";

let oauth = {
    client_id: "7342070",
    redirect_uri: "https://test-oauth-friends.firebaseapp.com/",
    scope: "friends",
    response_type: "token",
    revoke: 0
};
function buttonClicked(callback) {
    window.location.href = 'https://oauth.vk.com/authorize?client_id='+oauth.client_id+'&redirect_uri='+oauth.redirect_uri+'&scope='+oauth.scope+'&response_type='+oauth.response_type+'&revoke='+oauth.revoke+'';
    callback.call();
}
function redirect() {
    localStorage.setItem("redirect", JSON.stringify(true))
}
function handleButton() {
    buttonClicked(redirect())
}
class Oauth extends Component {
    render() {
        if (!JSON.parse(localStorage.getItem("redirect"))) {
            return (
                <div className="card-body">
                    <p className="card-text">При нажатии кнопки "Авторизоваться" будет выполнена OAuth авторизация через
                        Вконтакте и получены 5 рандомных друзей пользователя.</p>
                    <button className="btn btn-lg btn-primary" onClick={handleButton}>Авторизоваться</button>
                </div>
            )
        } else {
            return ( <Friend />)
        }
    }
}
export default Oauth