This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available URL

https://test-oauth-friends.firebaseapp.com/

### `npm install`
To install all the dependencies

### `npm start`
To start app on localhost:3000
